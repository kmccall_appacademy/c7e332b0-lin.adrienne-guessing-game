# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  num = rand(100) + 1
  count = 1
  puts 'Guess a number between 1 and 100.'
  guessed_num = gets.chomp.to_i

  while guessed_num != num
    count += 1
    comparison = guessed_num > num ? 'high' : 'low'

    puts "#{guessed_num} is too #{comparison}."
    puts 'Guess a new number between 1 and 100.'
    guessed_num = gets.chomp.to_i
  end

  puts "Congrats, you guessed the number #{num} after #{count} guesses!"
end

def file_shuffler(filename)
  base = File.basename(filename, ".*")
  ext = File.extname(filename)

  File.open("#{base}-shuffled#{ext}", 'w') do |file|
    File.readlines(filename).shuffle.each do |line|
      file.puts line.chomp
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  if ARGV.length == 1
    file_shuffler(ARGV[0])
  else
    puts "Enter a filename."
    filename = gets.chomp
    file_shuffler(filename)
  end
end
